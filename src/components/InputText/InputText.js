import React from 'react';
import { View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


function InputText(props) {
  const { iconName, type, ...TextInputProps } = props;

  return (
    <View style={{ flex: 1, flexDirection: 'row' }}>
      <View style={{ flex: 0.2 }}>
        <Icon name={iconName} size={24} color="gray" />
      </View>
      <TextInput
        {...TextInputProps}
      />
      {type === 'pwd' && (
        <View style={{ flex: 0.2 }}>
          <Icon name="eye" size={24} color="gray" />
        </View>
      )}
    </View>
  )
}