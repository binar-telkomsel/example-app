import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// import LandingScreen from './screens/LandingScreen/LandingScreen';
import LandingScreen from '../screens/LandingScreen/LandingScreen-hooks';
// import LoginScreen from './screens/LoginScreen/LoginScreen';
import LoginScreen from '../screens/LoginScreen/LoginScreen-hook';
import ProfileScreen from '../screens/ProfileScreen/ProfileScreen';
import HomePage from '../screens/HomePage/HomePage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

class MainNavigation extends React.Component {

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Landing" headerMode="none">
          <Stack.Screen
            name="Landing"
            component={LandingScreen}
          />
          <Stack.Screen
            name="Login"
            component={LoginScreen}
          />
          <Stack.Screen
            name="TabNavigator"
            component={TabNavigator}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#e91e63',
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomePage}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <Icon name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainNavigation;
